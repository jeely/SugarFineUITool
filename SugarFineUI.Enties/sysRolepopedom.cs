﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace SugarFineUI.Enties
{
    ///<summary>
    ///
    ///</summary>
    [SugarTable("sysRolepopedom")]
    public partial class sysRolepopedom
    {
           public sysRolepopedom(){


           }
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public int id {get;set;}

           /// <summary>
           /// Desc:权限角色名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string rname {get;set;}

           /// <summary>
           /// Desc:使用状态（默认1使用中，0不可使用）
           /// Default:1
           /// Nullable:True
           /// </summary>           
           public int? state {get;set;}

           /// <summary>
           /// Desc:关联sysmodule表id集合
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string moduleid {get;set;}

           /// <summary>
           /// Desc:是否假删除
           /// Default:0
           /// Nullable:True
           /// </summary>           
           public int? isdel {get;set;}

    }
}
