﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;

using FineUIPro;


namespace FineUIPro
{
    public static partial class FineUIProExtensions
    {

        /// <summary>
        /// 动态创建Grid结构，在 Page_Init事件里执行（不是Page_Load事件里）
        /// </summary>
        /// <param name="Grid1">The grid1.</param>
        /// <param name="dt">The dt.</param>
        public static void CreatGridStructByDataTable(this FineUIPro.Grid Grid1, DataTable dt)
        {
            FineUIPro.BoundField bf;

            for (int i = 0; i < dt.Columns.Count; i++)
            {
                bf = new FineUIPro.BoundField();
                bf.DataField = dt.Columns[i].Caption;
                bf.DataFormatString = "{0}";
                bf.HeaderText = dt.Columns[i].Caption;
                Grid1.Columns.Add(bf);
            }
        }

        /// <summary>
        /// 获取选中行的主id
        /// </summary>
        /// <param name="grid">FineUIPro.Grid</param>
        /// <param name="keyNumIndex">第几个主键（从0开始）</param>
        /// <returns></returns>
        public static string GetDataKeysBySelectedRow(this FineUIPro.Grid grid, int keyNumIndex = 0)
        {
            //需要正确设置grid DataKeyNames主键列名
            int[] selections = grid.SelectedRowIndexArray;//获取选中行
            if (selections.Length == 0)
            {
                return string.Empty;
            }

            StringBuilder sb = new StringBuilder();
            foreach (int rowIndex in selections)
            {
                sb.Append("," + int.Parse(grid.DataKeys[rowIndex][keyNumIndex].ToString()));
            }

            return sb.ToString().Substring(1);
        }

        /// <summary>
        /// 获取选中行的主id
        /// </summary>
        /// <param name="grid">FineUIPro.Grid</param>
        /// <param name="keyNumIndex">第几个主键（从0开始）</param>
        /// <returns></returns>
        public static List<int> GetDataKeysListBySelectedRow(this FineUIPro.Grid grid, int keyNumIndex = 0)
        {
            //需要正确设置grid DataKeyNames主键列名
            int[] selections = grid.SelectedRowIndexArray;//获取选中行
            if (selections.Length == 0)
            {
                return null;
            }

            List<int> list = new List<int>();
            foreach (int rowIndex in selections)
            {
                list.Add(int.Parse(grid.DataKeys[rowIndex][keyNumIndex].ToString()));//获取每行第keyNumIndex个主键
            }
            return list;
        }


        public static List<TGridViewModel> BindFineuiBySqlSugar<TGridViewModel>(this FineUIPro.Grid grid, SqlSugar.ISugarQueryable<TGridViewModel> queryable)
        {
            int recordCount = 0;

            List<TGridViewModel> data = queryable.ToPageList(grid.PageIndex, grid.PageSize, ref recordCount);

            grid.RecordCount = recordCount;
            grid.DataSource = data;
            grid.DataBind();
            return data;
        }

        

    }
}

