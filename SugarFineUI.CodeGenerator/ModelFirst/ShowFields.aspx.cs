﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SugarFineUI.CodeGenerator.code;
using SugarFineUI.CodeTemplates;
using FineUIPro;
using SqlSugar;
using SugarFineUI.Framework.Uitility;
using Template_ModelFirst = SugarFineUI.CodeTemplates.FineUIPro.Template_ModelFirst;

namespace SugarFineUI.CodeGenerator.ModelFirst
{
    public partial class ShowFields : CodeBasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ttbDB.Text = Server.MapPath("~/bin/SugarFineUI.Enties.dll");
            }
        }

        protected void Btn_SetSimpleCode_OnClick(object sender, EventArgs e)
        {
            //https://www.cnblogs.com/junjieok/p/4949806.html  获取特性和值
            Assembly assembly = Assembly.LoadFile(ttbDB.Text);
            //Type[] types = assembly.GetTypes();
            if (rbl_tables.SelectedItem==null || rbl_tables.SelectedValue.Length == 0)
            {
                NotifyError("请选择类名称！");
                return;
            }

            string tables = rbl_tables.SelectedValue;
            Type pType = assembly.GetExportedTypes().Where(p => p.Name== tables).OrderBy(p => p.Name).FirstOrDefault();//public类型 Model名称
            
            PropertyInfo[] propertyInfos;
            try
            {
                propertyInfos = pType.GetProperties(BindingFlags.Instance | BindingFlags.Public);//获取字段属性

                StringBuilder sb=new StringBuilder();
                foreach (PropertyInfo pi in propertyInfos)
                {
                    sb.AppendLine(string.Format("{0}={1}.{0},", pi.Name, txb_Alias.Text));
                }

                lb_Select.Text = ShowHighlightCode(sb.ToString());
            }
            catch (Exception exception)
            {
                NotifyError(exception.Message);
                return;
            }

            PageContext.RegisterStartupScript("ShowHighlight();");
        }

        
        private void BindTable()
        {
            //反射获取model名称
            //https://www.cnblogs.com/DHclly/p/9223326.html //反射获取注释
            //https://www.cnblogs.com/imyao/p/5263401.html
            Assembly assembly = Assembly.LoadFile(ttbDB.Text);
            //Type[] types = assembly.GetTypes();
            //assembly.GetExportedTypes();//public类型

            List<Type> list;
            
            list = assembly.GetExportedTypes().OrderBy(p => p.Name).ToList();

            rbl_tables.Items.Clear();
            for (int i = 0; i < list.Count; i++)
            {
                rbl_tables.Items.Add(new RadioItem(list[i].Name, list[i].Name));
            }
        }

        protected void btn_DBlink_OnClick(object sender, EventArgs e)
        {
            BindTable();
        }

        protected void ttbSearchTableName_OnTrigger1Click(object sender, EventArgs e)
        {
            BindTable();
        }

        protected void ttbSearchTableName_OnTrigger2Click(object sender, EventArgs e)
        {
            BindTable();
        }
    }
}

