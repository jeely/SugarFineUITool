﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SqlSugar ;

namespace SugarFineUI.CodeTemplates.FineUIPro.Template
{
    public class FineuiBase
    {
        /// <summary>
        /// 获取备注信息，没有备注则显示表名称
        /// </summary>
        /// <param name="comment">备注信息</param>
        /// <param name="name">表名称</param>
        /// <returns></returns>
        public static string GetComment(string comment, string name)
        {
            return string.IsNullOrEmpty(comment) ? name : comment ;
        }

        public static string GetNamespace2Str(string namespace2Str)
        {
            return namespace2Str.Length == 0 ? string.Empty : namespace2Str + "." ;
        }

        /// <summary>
        /// 生成实体生成代码
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        public static string GetModelCode(List<DbColumnInfo> list)
        {
            StringBuilder sb = new StringBuilder() ;
            for (int i = 0; i < list.Count; i++)
            {
                switch (list[i].DataType)
                {
                    case "bit":
                        sb.AppendLine("        ///<summary>") ;
                        sb.AppendLine("        ///" + list[i].ColumnDescription) ;
                        sb.AppendLine("        ///</summary>") ;
                        sb.AppendLine("        public bool " + (list[i].DbColumnName) + " { get; set; }") ;
                        sb.AppendLine("") ;
                        break ;

                    case "datetime":
                        sb.AppendLine("        ///<summary>") ;
                        sb.AppendLine("        ///" + list[i].ColumnDescription) ;
                        sb.AppendLine("        ///</summary>") ;
                        sb.AppendLine("        public DateTime " + (list[i].DbColumnName) + " { get; set; }") ;
                        sb.AppendLine("") ;
                        break ;

                    case "bigint":
                        sb.AppendLine("        ///<summary>") ;
                        sb.AppendLine("        ///" + list[i].ColumnDescription) ;
                        sb.AppendLine("        ///</summary>") ;
                        sb.AppendLine("        public Int64 " + (list[i].DbColumnName) + " { get; set; }") ;
                        sb.AppendLine("") ;
                        break ;

                    case "smallint": //break;
                    case "tinyint": //break;
                    case "int":
                        sb.AppendLine("        ///<summary>") ;
                        sb.AppendLine("        ///" + list[i].ColumnDescription) ;
                        sb.AppendLine("        ///</summary>") ;
                        sb.AppendLine("        public int " + (list[i].DbColumnName) + " { get; set; }") ;
                        sb.AppendLine("") ;
                        break ;

                    case "smallmoney": //break;
                    case "money": //break;
                    case "numeric": //break;
                    case "decimal":
                        sb.AppendLine("        ///<summary>") ;
                        sb.AppendLine("        ///" + list[i].ColumnDescription) ;
                        sb.AppendLine("        ///</summary>") ;
                        sb.AppendLine("        public Decimal " + (list[i].DbColumnName) + " { get; set; }") ;
                        sb.AppendLine("") ;
                        break ;

                    case "float":
                        sb.AppendLine("        ///<summary>") ;
                        sb.AppendLine("        ///" + list[i].ColumnDescription) ;
                        sb.AppendLine("        ///</summary>") ;
                        sb.AppendLine("        public Double " + (list[i].DbColumnName) + " { get; set; }") ;
                        sb.AppendLine("") ;
                        break ;

                    case "uniqueidentifier":
                        sb.AppendLine("        ///<summary>") ;
                        sb.AppendLine("        ///" + list[i].ColumnDescription) ;
                        sb.AppendLine("        ///</summary>") ;
                        sb.AppendLine("        public Guid " + (list[i].DbColumnName) + " { get; set; }") ;
                        sb.AppendLine("") ;
                        break ;

                    default:
                        sb.AppendLine("        ///<summary>") ;
                        sb.AppendLine("        ///" + list[i].ColumnDescription) ;
                        sb.AppendLine("        ///</summary>") ;
                        sb.AppendLine("        public string " + (list[i].DbColumnName) + " { get; set; }") ;
                        sb.AppendLine("") ;
                        break ;
                }

            }

            return sb.ToString() ;
        }
    }
}
