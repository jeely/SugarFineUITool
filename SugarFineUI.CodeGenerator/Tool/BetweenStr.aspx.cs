﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FineUIPro;
using SugarFineUI.CodeGenerator.code;
using SugarFineUI.Framework.Uitility;

namespace SugarFineUI.CodeGenerator
{
    public partial class BetweenStr : FineUIPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btn_showCode_OnClick(object sender, EventArgs e)
        {
            if (txb_A.Text.Length == 0)
            {
                NotifyError("第1个字符串不能为空字符串！");
                return;
            }

            if (rbl_line.SelectedValue == "1")
            {
                string info = txa_old.Text.Trim();
                string[] data = info.Split(Environment.NewLine.ToCharArray());
                StringBuilder sb = new StringBuilder() ;
                for (int i = 0; i < data.Length; i++)
                {
                    if(data[i].Length>0)
                        sb.AppendLine(StringHelper.GetBetweenStr(data[i], txb_A.Text, txb_B.Text, cb_IgnoreCase.Checked)) ;
                }

                txa_new.Text = sb.ToString() ;
            }
            else
            {
                txa_new.Text = StringHelper.GetBetweenStr(txa_old.Text, txb_A.Text, txb_B.Text,cb_IgnoreCase.Checked);
            }

           
        }

        

    }
}