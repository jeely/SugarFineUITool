# SugarFineUI代码生成工具

#### 介绍
使用sqlsugar+fineui生成数据底层代码和页面代码。包含常用的字符串拼接等工具和网站等

#### 使用手册
> 备用地址  https://www.jianshu.com/p/0901d8dde2c6


### 本工具简介
使用的SqlSugar 4.X，FineuiPro 框架，生成基本的页面和架构，适用于中小型项目快速开发。建议使用Vs2017，mssql数据库，Framework4.5以上的版本。
##### 关于sqlsugar：[http://www.codeisbug.com/Doc/8](http://www.codeisbug.com/Doc/8)
vs项目引用 ，在nuget中搜索并获取。

##### 关于FineuiPro：
由于版权的问题，请移步到知识星球上获取FineuiPro。[https://wx.zsxq.com/mweb/views/topicdetail/topicdetail.html?topic_id=244445115451141&user_id=48812121128128&share_from=ShareToWechat](https://wx.zsxq.com/mweb/views/topicdetail/topicdetail.html?topic_id=244445115451141&user_id=48812121128128&share_from=ShareToWechat)

###### SugarFineUI项目中，需要自己引用FineuiPro，本文省略此步骤。

> SugarFineUI开源地址 [https://gitee.com/sundayisblue/SugarFineUITool](https://gitee.com/sundayisblue/SugarFineUITool)


### 使用手册

1. 先生成SqlSugar实体。不想直接生成代码到项目中，怕覆盖原有代码，建议生成代码到其他位置，手动加入项目中。
![生成SqlSugar实体](https://gitee.com/uploads/images/2019/0423/184641_01f21ac7_436641.png)


2. 手动将生成的Enties文件放入到项目指定的位置

![Enties文件手动放入项目中](https://gitee.com/uploads/images/2019/0423/184641_353554a8_436641.png)

3. 生成FineuiPro页面代码，如图所示：
* 先在右边的数据库栏，连接数据库，选择要生成的表。 
* 点击生成简单代码按钮，在指定的位置生成代码。
* 可以在下面的显示单页代码按钮，查看生成代码。

![生成FineuiPro页面](https://gitee.com/uploads/images/2019/0423/184641_6c797343_436641.png)

4. admin是fineui代码，Enties是sqlsugar生成的orm实体

![生成的Fineui代码和Enties代码](https://images.gitee.com/uploads/images/2019/0423/094630_517485a4_436641.png)

5. 将admin放入到webTest 项目中(此项目只是用于测试和演示)，并转换为web应用程序。
![将生成的fineui代码放入演示环境](https://images.gitee.com/uploads/images/2019/0423/094630_9a59ed67_436641.png)

6. 运行生成的fineui代码。

![运行代码的演示效果](https://images.gitee.com/uploads/images/2019/0423/094625_e0568fd4_436641.png)


7. 以上步骤是dbfirst，先创建数据库，根据数据结构生成项目数据底层和fineui页面。SugarFineUI 暂时只支持mssql生成代码。如果你想用其他的数据库生成页面代码，请使用[反射实体]功能(又叫ModelFirst功能)，同样先有sqlsugar实体，也就是Enties层。如果你想显示Enties的注释内容，如下图，设置生成注释文件xml。需要重新生成解决方案。


![设置Enties生成注释xml文件](https://gitee.com/uploads/images/2019/0423/184641_9179e3fc_436641.png)

8. 反射实体(ModelFirst) 使用方式：同fineuipro生成代码类似，先反射dll文件，xml非必填。反射成功后，可以生成简单代码，和显示单页代码。步骤参考上面 “生成FineuiPro页面代码” (步骤3)。

![ModelFirst功能页面](https://gitee.com/uploads/images/2019/0423/184641_c8820c0e_436641.png)




#### QQ群：275110998

#### 如果觉得对您有所帮助，欢迎您捐赠

<img src="https://gitee.com/uploads/images/2019/0423/190419_15b3388e_436641.png" width="200" >

<img src="https://gitee.com/uploads/images/2019/0423/190842_1c6bdb00_436641.png" width="200" >