﻿using System;
using System.Collections.Generic;
using System.Configuration ;
using System.Data ;
using System.IO ;
using System.Linq;
using System.Text ;
using System.Web;
using System.Web.UI ;
using SugarFineUI.CodeTemplates ;
using SugarFineUI.CodeTemplates.FineUIPro.Template ;
using SugarFineUI.Framework.Uitility;
using Template_ModelFirst = SugarFineUI.CodeTemplates.FineUIPro.Template_ModelFirst ;
using FineUIPro ;
using SqlSugar ;

namespace SugarFineUI.CodeGenerator.code
{
    public partial class CodeBasePage : FineUIPage
    {
       
        public SqlSugarClient MyInstance()
        {
            var db = SugarFineUI.DBServices.DB_Base.Instance;
            if(Session["db"]!=null)
                db.CurrentConnectionConfig.ConnectionString = GetConnectionString();
            return db;
        }

        public string GetConnectionString()
        {
            return Session["db"] != null ? Session["db"].ToString() : ConfigurationManager.AppSettings["ConnectionString"] ;
        }

        /// <summary>
        ///  获取表信息
        /// </summary>
        /// <param name="likeName"></param>
        /// <returns></returns>
        public List<string> GetTableInfos(string likeName = "")
        {
            var db = MyInstance();

            if(likeName.Length==0)
            {
                return db.DbMaintenance.GetTableInfoList(true)
                //.Where(p=>p.Name!="sysAdmin" &&p.Name!= "sysModule" && p.Name!= "sysRolepopedom")
                .Select(p=>p.Name).ToList();
            }
            else
            {
                return db.DbMaintenance.GetTableInfoList(true)
                    //.Where(p => p.Name != "sysAdmin" && p.Name != "sysModule" && p.Name != "sysRolepopedom" )
                    .Where(p => p.Name.Contains(likeName))
                    .Select(p => p.Name).ToList();
            }
        }


        /// <summary>
        /// 获取字段信息集合，根据表
        /// </summary>
        /// <param name="tableName">表名称</param>
        /// <returns></returns>
        public List<DbColumnInfo> GetColumnDataByTableName(string tableName)
        {
            var db = MyInstance();
            var list = db.DbMaintenance.GetColumnInfosByTableName(tableName) ;
            foreach (var cm in list)
            {
                if (cm.ColumnDescription == null)
                {
                    cm.ColumnDescription = string.Empty ;
                }
                if (cm.DefaultValue == null)
                {
                    cm.DefaultValue = string.Empty ;
                }
            }
            return list;
        }

        /// <summary>
        /// 生成fineuiPro代码到指定的位置
        /// </summary>
        /// <param name="savePath">生成代码的物理地址</param>
        /// <param name="table">表信息实体</param>
        /// <param name="haveViewPage">是否生成view页面代码</param>
        public void CreateFineuiProCode(string savePath, TableModel table,bool haveViewPage)
        {
            if (!Directory.Exists(savePath))
            {
                Directory.CreateDirectory(savePath); //在根目录下建立文件夹
            }

            //add
            string templateInfo = FineuiAdd_cs.GetCode(table);
            File.WriteAllText(savePath + table.TableName + "Add.aspx.cs", templateInfo, Encoding.UTF8);

            templateInfo = FineuiAdd.GetCode(table);
            File.WriteAllText(savePath + table.TableName + "Add.aspx", templateInfo, Encoding.UTF8);

            //List
            templateInfo = FineuiList_cs.GetCode(table);
            File.WriteAllText(savePath + table.TableName + "List.aspx.cs", templateInfo, Encoding.UTF8);

            templateInfo = FineuiList.GetCode(table);
            File.WriteAllText(savePath + table.TableName + "List.aspx", templateInfo, Encoding.UTF8);

            //带视图页面
            if (haveViewPage)
            {
                //View
                templateInfo = FineuiView_cs.GetCode(table);
                File.WriteAllText(savePath + table.TableName + "View.aspx.cs", templateInfo, Encoding.UTF8);

                templateInfo = FineuiView.GetCode(table);
                File.WriteAllText(savePath + table.TableName + "View.aspx", templateInfo, Encoding.UTF8);
            }
        }

        /// <summary>
        /// 生成fineuiPro代码到指定的位置
        /// </summary>
        /// <param name="savePath">生成代码的物理地址</param>
        /// <param name="table">表信息实体</param>
        /// <param name="haveViewPage">是否生成view页面代码</param>
        public void CreateFineuiProCode_ModelFirst(string savePath, TableModel table,bool haveViewPage)
        {
            if (!Directory.Exists(savePath))
            {
                Directory.CreateDirectory(savePath); //在根目录下建立文件夹
            }

            //add
            string templateInfo = Template_ModelFirst.FineuiAdd_cs.GetCode(table);
            File.WriteAllText(savePath + table.TableName + "Add.aspx.cs", templateInfo, Encoding.UTF8);

            templateInfo = Template_ModelFirst.FineuiAdd.GetCode(table);
            File.WriteAllText(savePath + table.TableName + "Add.aspx", templateInfo, Encoding.UTF8);

            //List
            templateInfo = Template_ModelFirst.FineuiList_cs.GetCode(table);
            File.WriteAllText(savePath + table.TableName + "List.aspx.cs", templateInfo, Encoding.UTF8);

            templateInfo = Template_ModelFirst.FineuiList.GetCode(table);
            File.WriteAllText(savePath + table.TableName + "List.aspx", templateInfo, Encoding.UTF8);

            //带视图页面
            if (haveViewPage)
            {
                //View
                templateInfo = Template_ModelFirst.FineuiView_cs.GetCode(table);
                File.WriteAllText(savePath + table.TableName + "View.aspx.cs", templateInfo, Encoding.UTF8);

                templateInfo = Template_ModelFirst.FineuiView.GetCode(table);
                File.WriteAllText(savePath + table.TableName + "View.aspx", templateInfo, Encoding.UTF8);
            }
        }

        /// <summary>
        /// 获取高亮代码
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        public string ShowHighlightCode(string code)
        {
            return "<pre><code>" + StringHelper.HTML.TextBoxToHtml(code)+ "</code></pre>";
        }

    }
}