﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SugarFineUI.CodeTemplates
{
    public class Configure
    { 
        public Configure()
        { }

        /// <summary>
        /// 
        /// </summary>
        public int id { set ; get ; }

        /// <summary>
        /// 外键表
        /// </summary>
        public string tablename1 { set ; get ; }

        /// <summary>
        /// 外键字段
        /// </summary>
        public string columnkey { set ; get ; }

        /// <summary>
        /// 生成控件类型
        /// </summary>
        public string ctrltype { set ; get ; }

        /// <summary>
        /// 主键表
        /// </summary>
        public string tablename2 { set ; get ; }

        /// <summary>
        /// 主键默认显示字段
        /// </summary>
        public string column2 { set ; get ; }

        /// <summary>
        /// 主键表主键
        /// </summary>
        public string key2 { set ; get ; }
    }
}
