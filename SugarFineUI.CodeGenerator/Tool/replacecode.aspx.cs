﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FineUIPro;


namespace SugarFineUI.CodeGenerator.Tool
{
    public partial class replacecode : FineUIPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btn_showCode_OnClick(object sender, EventArgs e)
        {
            if (txb_replace.Text.Length == 0)
            {
                NotifyError("被替换的字符串不能为空字符串！");
                return;
            }
            txa_new.Text = txa_old.Text.Replace(txb_replace.Text, txb_newstr.Text);
        }

   

        protected void btn_delNullLine_OnClick(object sender, EventArgs e)
        {
            string[] data = txa_old.Text.Split(Environment.NewLine.ToCharArray());
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < data.Length; i++)
            {
                if (data[i].Trim().Length > 0)
                    sb.AppendLine(data[i].Trim());
            }

            txa_new.Text = sb.ToString() ;
        }
    }
}