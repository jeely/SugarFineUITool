﻿using System;
using System.Collections.Generic;
using System.Configuration ;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SugarFineUI.CodeGenerator.code;
using FineUIPro;

namespace SugarFineUI.CodeGenerator
{
    public partial class DbConfig : CodeBasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["db"] != null)
                {
                    ttbDB.Text = Session["db"].ToString() ;
                    BindTable() ;
                }
                else
                {
                    ttbDB.Text = ConfigurationManager.AppSettings["ConnectionString"] ;
                }
            }
        }

        protected void BindDB()
        {
            Session["db"] = ttbDB.Text ;

            try
            {
                var data = GetTableInfos();
                if (data == null || data.Count == 0)
                {
                    AlertError("获取不到数据库的table", false);
                    return;
                }

                Session["db"] = ttbDB.Text;
            }
            catch (Exception exception)
            {
                AlertError("连接数据库异常："+exception.Message,false);
                return;
            }
            //PageContext.RegisterStartupScript("parent.RefreshTables();");
            BindTable() ;
        }

        private void BindTable()
        {
            if (Session["db"] == null)
            {
                AlertError("请先连接数据库", false);
                return;
            }
            var data = GetTableInfos(ttbSearchTableName.Text);
            cbl_tables.BindByDataSource(data, "name", "name");
        }


        protected void btn_DBlink_OnClick(object sender, EventArgs e)
        {
            BindDB() ;
        }

        protected void ttbSearchTableName_OnTrigger1Click(object sender, EventArgs e)
        {
            BindTable();
        }

        protected void ttbSearchTableName_OnTrigger2Click(object sender, EventArgs e)
        {
            BindTable();
        }

        protected void btn_RefreshTables_OnClick(object sender, EventArgs e)
        {
            
        }

    }
}