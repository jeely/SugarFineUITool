﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FineUIPro;
 

namespace FineUIPro
{
    public static partial class FineUIProExtensions
    {
        #region Notify
        /// <summary>
        /// 显示通知对话框
        /// </summary>
        /// <param name="n"></param>
        /// <param name="message"></param>
        public static void ShowNotify(this Notify n, string message)
        {
            ShowNotify(n, message, MessageBoxIcon.Information);
        }

        /// <summary>
        /// 显示通知对话框
        /// </summary>
        /// <param name="nt"></param>
        /// <param name="message"></param>
        /// <param name="messageIcon">information, warning, question, error, success</param>
        public static void ShowNotify(this Notify nt, string message, MessageBoxIcon messageIcon)
        {
            nt.GetShowNotify();
            nt.MessageBoxIcon = messageIcon;
            nt.Show();
        }

        /// <summary>
        /// 获取默认消息框对象
        /// </summary>
        /// <param name="nt"></param>
        /// <returns></returns>
        public static void GetShowNotify(this Notify nt)
        {
            nt.Target = Target.Top;
            //nt.Message = message;
            nt.MessageBoxIcon = MessageBoxIcon.Information;
            nt.PositionX = Position.Center;
            nt.PositionY = Position.Top;
            nt.DisplayMilliseconds = 3000;
            nt.ShowHeader = false;
        }
        #endregion

        #region QQNotify
        /// <summary>
        /// 显示通知对话框
        /// </summary>
        /// <param name="nt"></param>
        /// <param name="message"></param>
        public static void ShowQQ(this Notify nt, string message)
        {
            ShowQQ(nt, message, MessageBoxIcon.Information);
        }

        /// <summary>
        /// 显示通知对话框
        /// </summary>
        /// <param name="nt"></param>
        /// <param name="message"></param>
        /// <param name="messageIcon"></param>
        public static void ShowQQ(this Notify nt, string message, MessageBoxIcon messageIcon)
        {
            GetShowQQNotify(nt);
            nt.Message = message;
            nt.MessageBoxIcon = messageIcon;
            nt.Show();
        }

        /// <summary>
        /// 获取qq显示消息框对象
        /// </summary>
        /// <param name="n"></param>
        /// <returns></returns>
        public static void GetShowQQNotify(this Notify n)
        {
            n.Target = Target.Top;
            //n.Message = message;
            n.MessageBoxIcon = MessageBoxIcon.Information;
            n.PositionX = Position.Right;
            n.PositionY = Position.Bottom;
            n.DisplayMilliseconds = 0;
            n.EnableClose = true;
            n.ShowHeader = false;
        }
        #endregion

        #region model

        /// <summary>
        /// 显示通知对话框
        /// </summary>
        /// <param name="nt"></param>
        /// <param name="message"></param> 
        public static void ShowModal(this Notify nt, string message)
        {
            ShowModal(nt, message, MessageBoxIcon.Information);
        }

        /// <summary>
        /// 显示通知对话框
        /// </summary>
        /// <param name="nt"></param>
        /// <param name="message"></param> 
        public static void ShowModal(this Notify nt, string message,string js)
        {
            nt.HideScript = js;
            ShowModal(nt, message, MessageBoxIcon.Information);
        }

        /// <summary>
        /// 显示通知对话框
        /// </summary>
        /// <param name="nt"></param>
        /// <param name="message"></param>
        /// <param name="messageIcon"></param>
        public static void ShowModal(this Notify nt, string message, MessageBoxIcon messageIcon)
        {
            nt.GetModal();
            nt.Message = message;
            nt.MessageBoxIcon = messageIcon;
            nt.Show();
        }

        /// <summary>
        /// 获取组态消息框对象
        /// </summary>
        /// <param name="n"></param>
        /// <returns></returns>
        public static void GetModal(this Notify n)
        {
            n.Target = Target.Top;
            //n.Message = message;
            n.MessageBoxIcon = MessageBoxIcon.Information;
            n.PositionX = Position.Center;
            n.PositionY = Position.Center;
            n.DisplayMilliseconds = 0;
            n.EnableClose = true;
            n.ShowHeader = true;
            n.IsModal = true;
        }

        #endregion

    }
}

