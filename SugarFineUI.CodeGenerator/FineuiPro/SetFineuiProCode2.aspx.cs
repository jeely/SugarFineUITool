﻿using System;
using System.Collections.Generic;
using System.IO ;
using System.Linq;
using System.Text ;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BoYuan.CodeGenerator.code ;
using BoYuan.CodeTemplates ;
using BoYuan.CodeTemplates.FineUIPro.Template ;

namespace BoYuan.CodeGenerator.FineuiPro
{
    public partial class SetFineuiProCode2 : CodeBasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Btn_SetSimpleCode_OnClick(object sender, EventArgs e)
        {
            var tables = hd_tables.Text.Split(',');
            if (hd_tables.Text.Length == 0 || tables.Length == 0)
            {
                NotifyError("获取表集合失败！");
                return;
            }



            foreach (string item in tables)
            {
                SetCodeByTableName(item);
            }

            NotifyInformation("生成简单代码成功!");
        }

        /// <summary>
        /// 生成页面代码，根据表
        /// </summary>
        /// <param name="tableName">表名</param>
        /// <returns></returns>
        private void SetCodeByTableName(string tableName)
        {
            try
            {
                List <ColumnModel>  cm= GetColumnDataByTableName(tableName);//字段信息集合

                string namespace2Str = txb_Path.Text.Trim().Replace("/", ".");

                string savePath =txb_savePath.Text.Trim()+ "\\" + txb_Path.Text + "\\" + tableName + "\\";
                if (!Directory.Exists(savePath))
                {
                    Directory.CreateDirectory(savePath); //在根目录下建立文件夹
                }

                TableModel table=new TableModel();
                table.Columns = cm ;
                table.ClassnameStr = txb_BaseClassName.Text ;
                table.NamespaceStr = txb_NameSpace.Text ;
                table.Namespace2Str = namespace2Str ;
                table.ModelName = txb_ModelName.Text ;
                table.TableName = tableName ;

                //add
                string templateInfo = FineuiAdd_cs.GetCode(table) ; 
                File.WriteAllText(savePath + tableName + "Add.aspx.cs", templateInfo, Encoding.UTF8);

                templateInfo = FineuiAdd.GetCode(table);
                File.WriteAllText(savePath + tableName + "Add.aspx", templateInfo, Encoding.UTF8);

                //List
                templateInfo = FineuiList_cs.GetCode(table);
                File.WriteAllText(savePath + tableName + "List.aspx.cs", templateInfo, Encoding.UTF8);

                templateInfo = FineuiList.GetCode(table);
                File.WriteAllText(savePath + tableName + "List.aspx", templateInfo, Encoding.UTF8);

                //带视图页面
                if (cb_isHaveViewPage.Checked)
                {
                    //View
                    templateInfo = FineuiView_cs.GetCode(table);
                    File.WriteAllText(savePath + tableName + "View.aspx.cs", templateInfo, Encoding.UTF8);

                    templateInfo = FineuiView.GetCode(table);
                    File.WriteAllText(savePath + tableName + "View.aspx", templateInfo, Encoding.UTF8);
                }
            }
            catch (Exception ex)
            {
                NotifyError("生成代码有问题：表名称("+ tableName + ")" + ex.Message);
            }
        }


        protected void Btn_AddPageInfoToDB_OnClick(object sender, EventArgs e)
        {
            string sql = string.Format(@"
                            WITH cte AS (
                                     SELECT syso.[name]
                                     FROM   sysobjects AS syso
                                     WHERE  syso.xtype = 'u'
                                            AND syso.[name] NOT IN ('sysadmin', 'sysModule', 'sysRolepopedom')
                                            AND NOT EXISTS
                                                (
                                                    SELECT 1
                                                    FROM   sysModule AS sm
                                                    WHERE  sm.moduleUrl LIKE '%/' + syso.name + 'list.aspx'
                                                )
                                 )

                            INSERT INTO sysModule
                              (
                                parentID,
                                modulePath,
                                moduleName,
                                moduleUrl,
                                moduleIcon,
                                sort,
                                [status],
                                info
                              )
                            SELECT *
                            FROM   (
                                       SELECT 0       AS parentID,
                                              '0,'    AS modulePath,
                                              [name]  AS moduleName,
                                              [name] + '/' + [name] + 'add.aspx' AS moduleUrl,
                                              ''      AS moduleIcon,
                                              1000    AS sort,
                                              0       AS [status],
                                              ''      AS info
                                       FROM   cte
                                       UNION ALL 
                                       SELECT 0       AS parentID,
                                              '0,'    AS modulePath,
                                              [name]  AS moduleName,
                                               [name] + '/' + [name] + 'List.aspx' AS moduleUrl,
                                              ''      AS moduleIcon,
                                              1000    AS sort,
                                              0       AS [status],
                                              ''      AS info
                                       FROM   cte
                                   ) AS temptable
                            ORDER BY
                                   moduleName
                            ");

            var db = MyInstance();
            int tempInt = db.Ado.ExecuteCommand(sql);

            NotifyInformation(string.Format("添加了{0}条数据，请根据业务逻辑更改页面组件数据!", tempInt));
        }

        protected void btn_ShowCode_OnClick(object sender, EventArgs e)
        {
          
        }
    }
}