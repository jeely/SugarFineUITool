﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text ;
using System.Text.RegularExpressions ;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FineUIPro ;
using SugarFineUI.CodeGenerator.Tool ;
using SugarFineUI.Framework.Uitility;


namespace SugarFineUI.CodeGenerator.Tool.ApiHelper
{
    public partial class post_Helper : FineUIPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                txa_old.EmptyText = @"
报文形式
﻿20181015 17:04:16:074 POST   TranType=0004
﻿20181015 17:04:16:074 POST   OrderStatus=0000
﻿20181015 17:04:16:074 POST   TranDate=20181015
" ;
            }
        }

        protected void btn_par_OnClick(object sender, EventArgs e)
        {
            string[] data = txa_old.Text.Split(Environment.NewLine.ToCharArray());
         
            List<Param> list=new List<Param>();
            Param par ;
            string key, value ;
            for (int i = 0; i < data.Length; i++)
            {
                if (data[i].Length > 0)
                {
                     StringHelper.GetKeyAndValue(data[i], out key, out value, txb_punctuation.Text) ;

                    par = new Param() ;
                    par.keyStr = key.Trim().Split(' ').Last();
                    par.valueStr = value.Trim().Split(' ').First();
                    list.Add(par) ;
                }
            }

            ViewState["data"] = list;
            bind() ;
        }
        

        protected void btn_add_OnClick(object sender, EventArgs e)
        {
            //添加参数
            List<Param> list = new List<Param>();
            if (ViewState["data"] != null)
            {
                list = ViewState["data"] as List<Param>;
            }

            list.Add(new Param() { keyStr = txb_key.Text.Trim(), valueStr = txb_value.Text.Trim() });

            ViewState["data"] = list;
            bind();

            txb_key.Text = string.Empty ;
            txb_value.Text = string.Empty ;

        }

        protected void btn_post_OnClick(object sender, EventArgs e)
        {
            string url = txb_url.Text.Trim();
            if (url == "http://" || url == "https://")
            { NotifyError("要提交的url地址格式正确！");return;}

            List<Param> list = ViewState["data"] as List<Param>;

            if (list != null && list.Count > 0)
            {
                StringBuilder strHtml = new StringBuilder();
                strHtml.AppendLine("<html>");
                strHtml.AppendLine("<body>");
                strHtml.AppendLine("<head><title>POST 提交 - 跳转中...</title></head>");
                strHtml.AppendLine(string.Format("<form id=\"postFrom\" name=\"postFrom\" method=\"{0}\" action=\"{1}\">", "POST", url));
                foreach (var t in list)
                {
                    strHtml.AppendLine(string.Format("<input type=\"hidden\" name=\"{0}\" value='{1}'/>", t.keyStr, t.valueStr));
                }
                strHtml.AppendLine("</form>");
                strHtml.AppendLine("<script>");
                strHtml.AppendLine("document.forms['postFrom'].submit(); ");
      
                strHtml.AppendLine("</script>");
                strHtml.AppendLine("</body>");
                strHtml.AppendLine("</html>");

                txa_html.Text = strHtml.ToString() ;
                //PageContext.RegisterStartupScript("replaceValue();");
            }
            else
            {
                Alert.Show("先获取要提交的参数");
                return;
            }

            


            return;

            //post提交
            

            StringBuilder sb=new StringBuilder();
           
            string parStr = string.Empty ;
            if (list!=null && list.Count > 0)
            {
                foreach (var t in list)
                {
                    sb.AppendFormat("&{0}={1}", t.keyStr, t.valueStr);
                }
                parStr = sb.ToString().Substring(1) ;
            }
            else
            {
                Alert.Show("先获取要提交的参数");
                return;
            }

            string res= HttpHelper.PostRemotePage(txb_url.Text.Trim(), parStr) ;

            Alert.Show(res);
        }

        private void bind()
        {
            if (ViewState["data"] != null)
            {
                Grid1.DataSource = ViewState["data"] as List<Param> ;
                Grid1.DataBind(); 
            }
        }

       

        protected void lbtnDel_OnCommand(object sender, CommandEventArgs e)
        {
            string val = e.CommandArgument.ToString() ;
            if (ViewState["data"] != null)
            {
                List<Param> list=ViewState["data"] as List<Param>;
                list.RemoveAll(x => x.keyStr == val);

                ViewState["data"] = list ;

            }

            bind();
        }


        protected void btn_ParamUrl_OnClick(object sender, EventArgs e)
        {
            string url = txb_url.Text.Trim();
            if (url == "http://" || url == "https://")
            { NotifyError("要提交的url地址格式正确！"); return; }

            List<Param> list = ViewState["data"] as List<Param>;

            if (list != null && list.Count > 0)
            {
                StringBuilder strHtml = new StringBuilder();
               
                foreach (var t in list)
                {
                    strHtml.AppendFormat("&{0}={1}", t.keyStr, t.valueStr);
                }

                txb_Paramurl.Text = url + "?"+strHtml.ToString().Substring(1);
                //PageContext.RegisterStartupScript("replaceValue();");
            }
            else
            {
                Alert.Show("先获取要提交的参数");
                return;
            }
        }

        protected void btn_signCode_OnClick(object sender, EventArgs e)
        {
            string[] parameStrings = txb_signUrl.Text.Split('&') ;

            StringBuilder sb_string=new StringBuilder();
            StringBuilder sb_parame = new StringBuilder();
            StringBuilder sb_parameValue = new StringBuilder();

            StringBuilder sb_keyAndValue = new StringBuilder();

            string key ;
            for (int i = 0; i < parameStrings.Length; i++)
            {
                key = parameStrings[i].Split('=')[0] ;
                sb_string.AppendLine("string " + key + "=string.Empty;") ;
                sb_parame.AppendFormat("&{0}={{{1}}}", key, i) ;
                sb_parameValue.Append("," + key) ;

                sb_keyAndValue.Append("+\"&"+key+"=\"+"+key);
            }
            
            txa_signCode.Text = sb_string.ToString() + 
                                string.Format("string signData=string.Format(\"{0}\"{1});",sb_parame.ToString().Substring(1),sb_parameValue.ToString()) 
                + Environment.NewLine+
                               "string signData=\"" + sb_keyAndValue.ToString().Substring(3)+";";
        }
    }
    /// <summary>
    /// 参数实体
    /// </summary>
    [Serializable]
    public class Param
    {
        public string keyStr { get ; set ; }
        public string valueStr { get ; set ; }
    }
}