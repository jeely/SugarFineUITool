﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net ;
using System.Security.Cryptography ;
using System.Text;
using System.Text.RegularExpressions ;
using System.Threading.Tasks;
using System.Web ;

namespace SugarFineUI.Framework.Uitility
{
    /// <summary>
    /// 字符串相关工具类(加密解密等)
    /// </summary>
    public partial class StringHelper
    {
        /// <summary> 
        /// 解密数据 
        /// </summary> 
        /// <param name="Text"></param> 
        /// <param name="sKey"></param> 
        /// <returns></returns> 
        public static string Decrypt(string Text, string sKey)
        {
            DESCryptoServiceProvider des = new DESCryptoServiceProvider();
            int len;
            len = Text.Length / 2;
            byte[] inputByteArray = new byte[len];
            int x, i;
            for (x = 0; x < len; x++)
            {
                i = Convert.ToInt32(Text.Substring(x * 2, 2), 16);
                inputByteArray[x] = (byte)i;
            }
            des.Key = ASCIIEncoding.ASCII.GetBytes(System.Web.Security.FormsAuthentication.HashPasswordForStoringInConfigFile(sKey, "md5").Substring(0, 8));
            des.IV = ASCIIEncoding.ASCII.GetBytes(System.Web.Security.FormsAuthentication.HashPasswordForStoringInConfigFile(sKey, "md5").Substring(0, 8));
            System.IO.MemoryStream ms = new System.IO.MemoryStream();
            CryptoStream cs = new CryptoStream(ms, des.CreateDecryptor(), CryptoStreamMode.Write);
            cs.Write(inputByteArray, 0, inputByteArray.Length);
            cs.FlushFinalBlock();
            return Encoding.Default.GetString(ms.ToArray());
        }
        #region 生成随机数
        public static string GetNumPwd(int num)//生成数字随机数
        {
            string a = "0123456789";
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < num; i++)
            {
                sb.Append(a[new Random(Guid.NewGuid().GetHashCode()).Next(0, a.Length - 1)]);
            }

            return sb.ToString();
        }
        #endregion


        /// <summary>
        /// 获取两个字符串中间的字符串
        /// </summary>
        /// <param name="str">要处理的字符串,例ABCD</param>
        /// <param name="str1">第1个字符串,例AB</param>
        /// <param name="str2">第2个字符串,例D</param>
        /// <param name="isIgnoreCase">是否忽略大小</param>
        /// <returns>例返回C</returns>
        public static string GetBetweenStr(string str, string str1, string str2, bool isIgnoreCase = true)
        {
            int i1 = isIgnoreCase ? str.IndexOf(str1, StringComparison.InvariantCultureIgnoreCase) : str.IndexOf(str1);
            if (i1 < 0) //找不到返回空
            {
                return "";
            }

            int i2 = isIgnoreCase ? str.IndexOf(str2, i1 + str1.Length, StringComparison.InvariantCultureIgnoreCase) : str.IndexOf(str2, i1 + str1.Length); //从找到的第1个字符串后再去找
            if (i2 < 0) //找不到返回空
            {
                return "";
            }

            return str.Substring(i1 + str1.Length, i2 - i1 - str1.Length);
        }


        /// <summary>
        /// 替换字符串中间多余(包括tab)的空格
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string ReplaceExcessSpace(string str)
        {
            return Regex.Replace(str, @"[ ]+|\t", " ");
        }


        /// <summary>
        /// 根据分割符号，获取key和value。例如:分割符 : ,Referer: https://123.com 获取 Referer，和https://123.com
        /// </summary>
        /// <param name="info"></param>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <param name="indexOfChar">分割符号</param>
        public static void GetKeyAndValue(string info, out string key, out string value, string indexOfChar = "=")
        {
            int startIndex = info.IndexOf(indexOfChar);//获取第一个等号的位置
            if (startIndex < 0)
            {
                key = string.Empty;
                value = string.Empty;
                return;
            }

            key = info.Substring(0, startIndex);//获取分组最后一个字符串，就是key
            value = info.Substring(startIndex + indexOfChar.Length);
        }



    }
}
