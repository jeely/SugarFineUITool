﻿using System;
using System.Collections.Generic;
using System.IO ;
using System.Linq;
using System.Text ;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SugarFineUI.CodeGenerator.code ;
using SugarFineUI.CodeTemplates ;
using SugarFineUI.CodeTemplates.FineUIPro.Template ;
using SugarFineUI.Framework.Uitility ;
using FineUIPro;
using SqlSugar ;

namespace SugarFineUI.CodeGenerator.FineuiPro
{
    public partial class SetFineuiProCode : CodeBasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Btn_SetSimpleCode_OnClick(object sender, EventArgs e)
        {
            var tables = hd_tables.Text.Split(',');
            if (hd_tables.Text.Length == 0 || tables.Length == 0)
            {
                NotifyError("获取表集合失败！");
                return;
            }

            try
            {
                foreach (string item in tables)
                {
                    SetCodeByTableName(item);
                }
            }
            catch (Exception exception)
            {
                NotifyError(exception.Message);
                return;
            }

            NotifyInformation("生成简单代码成功!");
        }

        /// <summary>
        /// 生成页面代码，根据表
        /// </summary>
        /// <param name="tableName">表名</param>
        /// <returns></returns>
        private void SetCodeByTableName(string tableName)
        {
            try
            {
                List<DbColumnInfo> cm = GetColumnDataByTableName(tableName);//字段信息集合

                string namespace2Str = txb_Path.Text.Trim().Replace("/", ".");

                string savePath =txb_savePath.Text.Trim()+ "\\" + txb_Path.Text + "\\" + tableName + "\\";

                TableModel table=new TableModel();
                table.Columns = cm ;
                table.ClassnameStr = txb_BaseClassName.Text ;
                table.NamespaceStr = txb_NameSpace.Text ;
                table.Namespace2Str = namespace2Str ;
                table.ModelName = txb_ModelName.Text ;
                table.TableName = tableName ;

                CreateFineuiProCode(savePath, table, cb_isHaveViewPage.Checked);
            }
            catch (Exception ex)
            {
                throw new Exception("生成代码有问题：表名称(" + tableName + ")" + ex.Message);
            }
        }


        protected void Btn_AddPageInfoToDB_OnClick(object sender, EventArgs e)
        {
            // todo 改成sugar版
            string sql = string.Format(@"
                            WITH cte AS (
                                     SELECT syso.[name]
                                     FROM   sysobjects AS syso
                                     WHERE  syso.xtype = 'u'
                                            AND syso.[name] NOT IN ('sysadmin', 'sysModule', 'sysRolepopedom')
                                            AND NOT EXISTS
                                                (
                                                    SELECT 1
                                                    FROM   sysModule AS sm
                                                    WHERE  sm.moduleUrl LIKE '%/' + syso.name + 'list.aspx'
                                                )
                                 )

                            INSERT INTO sysModule
                              (
                                parentID,
                                modulePath,
                                moduleName,
                                moduleUrl,
                                moduleIcon,
                                sort,
                                [status],
                                info
                              )
                            SELECT *
                            FROM   (
                                       SELECT 0       AS parentID,
                                              '0,'    AS modulePath,
                                              [name]  AS moduleName,
                                              [name] + '/' + [name] + 'add.aspx' AS moduleUrl,
                                              ''      AS moduleIcon,
                                              1000    AS sort,
                                              0       AS [status],
                                              ''      AS info
                                       FROM   cte
                                       UNION ALL 
                                       SELECT 0       AS parentID,
                                              '0,'    AS modulePath,
                                              [name]  AS moduleName,
                                               [name] + '/' + [name] + 'List.aspx' AS moduleUrl,
                                              ''      AS moduleIcon,
                                              1000    AS sort,
                                              0       AS [status],
                                              ''      AS info
                                       FROM   cte
                                   ) AS temptable
                            ORDER BY
                                   moduleName
                            ");

            var db = MyInstance();
            int tempInt = db.Ado.ExecuteCommand(sql);

            NotifyInformation(string.Format("添加了{0}条数据，请根据业务逻辑更改页面组件数据!", tempInt));
        }

        protected void btn_ShowCode_OnClick(object sender, EventArgs e)
        {
            //显示单表FineuiPro代码
            var tables = hd_tables.Text.Split(',');
            if (hd_tables.Text.Length == 0 || tables.Length == 0)
            {
                NotifyError("获取表集合失败！");
                return;
            }
            string namespace2Str = txb_Path.Text.Trim().Replace("/", ".");

            var tableName = tables[0] ;
            List<DbColumnInfo> cm = GetColumnDataByTableName(tableName);//字段信息集合  TableModel table=new TableModel();
            TableModel table = new TableModel();
            table.Columns = cm;
            table.ClassnameStr = txb_BaseClassName.Text;
            table.NamespaceStr = txb_NameSpace.Text;
            table.Namespace2Str = namespace2Str;
            table.ModelName = txb_ModelName.Text;
            table.TableName = tableName;

            //add
            string templateInfo = FineuiAdd_cs.GetCode(table);
            lb_addCS.Text = ShowHighlightCode(templateInfo);

            templateInfo = FineuiAdd.GetCode(table);
            lb_addAspx.Text = ShowHighlightCode(templateInfo);

            //List
            templateInfo = FineuiList_cs.GetCode(table);
            lb_listCS.Text = ShowHighlightCode(templateInfo);

            templateInfo = FineuiList.GetCode(table);
            lb_listAspx.Text = ShowHighlightCode(templateInfo);

            //View
            templateInfo = FineuiView_cs.GetCode(table);
            lb_viewCS.Text = ShowHighlightCode(templateInfo);

            templateInfo = FineuiView.GetCode(table);
            lb_viewAspx.Text = ShowHighlightCode(templateInfo);

            PageContext.RegisterStartupScript("ShowHighlight();");
        }
    }
}