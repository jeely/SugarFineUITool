﻿using System;
using System.Collections ;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Caching;

namespace SugarFineUI.DBServices
{
    /// <summary>
    /// Cache 通用操作类
    /// 1、IsExist(string cacheKey)，判断缓存对象是否存在
    /// 2、GetCache(string cacheKey)，获取数据缓存
    /// 3、GetCache<T>(string cacheKey)，获取缓存对象
    /// 4、InsertFile(string cacheKey, object cacheValue, string fileName)，创建缓存项的文件依赖
    /// 5、SetCache(string cacheKey, object cacheValue)，设置数据缓存
    /// 6、SetCache(string cacheKey, object cacheValue, TimeSpan timeOut)，设置数据缓存，并设置超时时间（多长时间后超时）
    /// 7、SetCache(string cacheKey, object cacheValue, int iExpires)，创建缓存项过期，过期时间(分钟数)
    /// 8、SetCache(string cacheKey, object cacheValue, DateTime iExpires, TimeSpan tExpires)，设置数据缓存，并设置过期时间和超时时间
    /// 9、RemoveCache(string cacheKey)，移除指定数据缓存
    /// 10、RemoveAllCache()，移除全部缓存
    /// </summary>
    public class CacheHelper
    {
        #region 获取设置
        /// <summary>
        /// 判断缓存对象是否存在
        /// </summary>
        /// <param name="strKey">缓存键值名称</param>
        /// <returns>是否存在true 、false</returns>
        public static bool IsExist(string cacheKey)
        {
            return HttpContext.Current.Cache[cacheKey] != null;
        }
        /// <summary>
        /// 获取数据缓存
        /// </summary>
        /// <param name="cacheKey">缓存主键</param>
        public static object GetCache(string cacheKey)
        {
            System.Web.Caching.Cache objCache = HttpRuntime.Cache;
            return objCache[cacheKey];
        }
        /// <summary>
        /// 获取缓存对象
        /// </summary>
        /// <typeparam name="T">T对象（泛型）</typeparam>
        /// <param name="cacheKey">缓存Key</param>
        /// <returns></returns>
        public static T GetCache<T>(string cacheKey)
        {
            object obj = GetCache(cacheKey);
            return obj == null ? default(T) : (T)obj;
        }
        #endregion

        #region 缓存设置
        /// <summary>
        /// 创建缓存项的文件依赖
        /// </summary>
        /// <param name="cacheKey">缓存Key</param>
        /// <param name="cacheValue">object对象</param>
        /// <param name="fileName">文件绝对路径</param>
        public static void InsertFile(string cacheKey, object cacheValue, string fileName)
        {
            //创建缓存依赖项
            CacheDependency dep = new CacheDependency(fileName);
            //创建缓存
            HttpContext.Current.Cache.Insert(cacheKey, cacheValue, dep);
        }
        /// <summary>
        /// 设置数据缓存
        /// </summary>
        /// <param name="cacheKey">缓存主键</param>
        /// <param name="cacheValue">缓存值</param>
        public static void SetCache(string cacheKey, object cacheValue)
        {
            System.Web.Caching.Cache objCache = HttpRuntime.Cache;
            objCache.Insert(cacheKey, cacheValue);
        }
        /// <summary>
        /// 设置数据缓存
        /// </summary>
        /// <param name="cacheKey">缓存主键</param>
        /// <param name="cacheValue">缓存值</param>
        /// <param name="timeOut">缓存超时时间</param>
        public static void SetCache(string cacheKey, object cacheValue, TimeSpan timeOut)
        {
            System.Web.Caching.Cache objCache = HttpRuntime.Cache;
            objCache.Insert(cacheKey, cacheValue, null, DateTime.MaxValue, timeOut, System.Web.Caching.CacheItemPriority.NotRemovable, null);
        }
        /// <summary>
        /// 创建缓存项过期，过期时间(分钟)
        /// </summary>
        /// <param name="cacheKey">缓存Key</param>
        /// <param name="cacheValue">object对象</param>
        /// <param name="iExpires">过期时间(分钟)</param>
        public static void SetCache(string cacheKey, object cacheValue, int iExpires)
        {
            HttpContext.Current.Cache.Insert(cacheKey, cacheValue, null, Cache.NoAbsoluteExpiration, new TimeSpan(0, iExpires, 0));
        }
        /// <summary>
        /// 设置数据缓存
        /// </summary>
        /// <param name="cacheKey">缓存主键</param>
        /// <param name="cacheValue">缓存值</param>
        /// <param name="iExpires">到期时间</param>
        /// <param name="tExpires">移除缓存时间</param>
        public static void SetCache(string cacheKey, object cacheValue, DateTime iExpires, TimeSpan tExpires)
        {
            System.Web.Caching.Cache objCache = HttpRuntime.Cache;
            objCache.Insert(cacheKey, cacheValue, null, iExpires, tExpires);
        }
        #endregion

        #region 缓存清理
        /// <summary>
        /// 移除指定数据缓存
        /// </summary>
        /// <param name="cacheKey">缓存主键</param>
        public static void RemoveCache(string cacheKey)
        {
            System.Web.Caching.Cache _cache = HttpRuntime.Cache;
            _cache.Remove(cacheKey);
        }
        /// <summary>
        /// 移除全部缓存
        /// </summary>
        public static void RemoveAllCache()
        {
            System.Web.Caching.Cache _cache = HttpRuntime.Cache;
            IDictionaryEnumerator CacheEnum = _cache.GetEnumerator();
            while (CacheEnum.MoveNext())
            {
                _cache.Remove(CacheEnum.Key.ToString());
            }
        }
        #endregion

        #region 简单缓存操作
        /// <summary>
        /// 创建缓存项的文件
        /// </summary>
        /// <param name="key">缓存Key</param>
        /// <param name="obj">object对象</param>
        public static void Insert(string key, object obj)
        {
            //创建缓存
            HttpContext.Current.Cache.Insert(key, obj);
        }
        /// <summary>
        /// 移除缓存项的文件
        /// </summary>
        /// <param name="key">缓存Key</param>
        public static void Remove(string key)
        {
            //创建缓存
            HttpContext.Current.Cache.Remove(key);
        }

        /// <summary>
        /// 创建缓存项的文件依赖
        /// </summary>
        /// <param name="key">缓存Key</param>
        /// <param name="obj">object对象</param>
        /// <param name="fileName">文件绝对路径</param>
        public static void Insert(string key, object obj, string fileName)
        {
            //创建缓存依赖项
            CacheDependency dep = new CacheDependency(fileName);
            //创建缓存
            HttpContext.Current.Cache.Insert(key, obj, dep);
        }
        /// <summary>
        /// 创建缓存项过期
        /// </summary>
        /// <param name="key">缓存Key</param>
        /// <param name="obj">object对象</param>
        /// <param name="expires">过期时间(分钟)</param>
        public static void Insert(string key, object obj, int expires)
        {
            HttpContext.Current.Cache.Insert(key, obj, null, Cache.NoAbsoluteExpiration, new TimeSpan(0, expires, 0));
        }
        /// <summary>
        /// 获取缓存对象
        /// </summary>
        /// <param name="key">缓存Key</param>
        /// <returns>object对象</returns>
        public static object Get(string key)
        {
            if (string.IsNullOrEmpty(key))
            {
                return null;
            }
            return HttpContext.Current.Cache.Get(key);
        }
        /// <summary>
        /// 获取缓存对象
        /// </summary>
        /// <typeparam name="T">T对象</typeparam>
        /// <param name="key">缓存Key</param>
        /// <returns></returns>
        public static T Get<T>(string key)
        {
            object obj = Get(key);
            return obj == null ? default(T) : (T)obj;
        }
        /// <summary>
        /// 本地缓存写入（默认缓存20min）
        /// </summary>
        /// <param name="name">key</param>
        /// <param name="value">value</param>
        public static void Set(string name, object value)
        {
            Set(name, value, null);
        }
        /// <summary>
        /// 本地缓存写入（默认缓存20min）,依赖项
        /// </summary>
        /// <param name="name">key</param>
        /// <param name="value">value</param>
        /// <param name="cacheDependency">依赖项</param>
        public static void Set(string name, object value, CacheDependency cacheDependency)
        {
            HttpRuntime.Cache.Insert(name, value, cacheDependency, Cache.NoAbsoluteExpiration, TimeSpan.FromMinutes(20));
        }
        #endregion
    }
}
